package unidadiii.movil.programacion.sumadorsencillo;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class SumadorActivity extends Activity {
    public EditText txtnum1;
    public EditText txtnum2;
    public  EditText txtres;
    public Button btnvalida;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sumador);
        txtnum1=(EditText)findViewById(R.id.txtnum1);
        txtnum2=(EditText)findViewById(R.id.txtnum2);
        txtres=(EditText)findViewById(R.id.txtres);
        btnvalida=(Button)findViewById(R.id.btnvalida);
        /// accion del boton  meainte Java (Listener)
        btnvalida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num1Leido=txtnum1.getText().toString();
                int num1=Integer.parseInt(num1Leido);
                String num2Leido=txtnum2.getText().toString();
                int num2=Integer.parseInt(num2Leido);
                int res=num1+num2;
                txtres.setText(String.valueOf(res));

            }
        });
    }


}
